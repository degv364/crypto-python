import datetime


def get_ISO_now():
    return datetime.datetime.now()


def get_ISO_datetime():
    return datetime.datetime.now().isoformat(sep=" ")


def get_datetime_from_ISO(datetime):
    return datetime.datetime.fromisoformat(datetime)


def age(birth, now):
    birthdate = datetime.date(
        year=birth[0], month=birth[1], day=birth[2])
    today = datetime.data(
        year=now[0], month=now[1], day=now[2])

    delta = (today-birthdate).days
    return int(delta/365.25)
