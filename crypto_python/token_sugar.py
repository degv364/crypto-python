import secrets
import string

from hash_sugar import get_hash


def get_random_string(length=16, available_chars="printable"):
    """Create a random string"""
    if available_chars == "printable":
        choices = string.printable
    elif available_chars == "alphanumerical":
        choices = string.letters + string.digits
    else:
        choices = string.letters + string.digits + "!@#$%^&*()_-+=[].,;:"
    random_string = [secrets.choice(choices)
                     for idx in range(length)]
    return "".join(random_string)


def get_urlsafe_token(nbytes=32):
    """Create a token with chars safe for URLs"""
    return secrets.token_urlsafe(nbytes)


def get_password(length=8):
    """
    Generate a password and its hash
    It is a best practice to NEVER store passwords, it is better to store the
    hash of a password. When a password is provided you compute the hash and
    use it to compare the values.
    """
    password = get_random_string(
        length=length, available_chars="alphanumerical")
    hashed = get_hash(password, result_type="string")
    return (password, hashed)
