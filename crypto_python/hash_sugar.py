import hashlib


def get_hash(input_data, hash_type="sha256", result_type="bytes"):
    """Hash some data"""
    hasher = hashlib.new(hash_type)
    hasher.update(input_data)
    if result_type == "string":
        result = hasher.hexdigest()
    else:
        result = hasher.digest()
    return result


def get_hash_batch(input_iterable,
                   hash_type="sha256",
                   result_type="bytes"):
    """Hash a group of things. Returns a map in case it has to be lazy"""
    return map(
        lambda item: get_hash(
            item,
            hash_type=hash_type,
            result_type=result_type),
        input_iterable)


def hash_inputs(*args, hash_type="sha256", result_type="bytes"):
    aggregate = "".join([str(arg) for arg in args])
    return get_hash(aggregate)


def get_compressed_hash(input_data, result_type="bytes"):
    """Create a compressed hash"""
    long_hash = get_hash(input_data, hash_type="sha1")
    length = len(long_hash)
    short_hash = []
    for idx in range(int(length/4)):
        partial = int(0.25 * (
            int(long_hash[idx]) +
            int(long_hash[idx+1]) +
            int(long_hash[idx+2]) +
            int(long_hash[idx+3]))
        )
        if result_type == "string":
            word = hex(partial)[2:]
            if len(word) == 1:
                word = "0" + word
            short_hash.append(word)
        else:
            short_hash.append(partial)

    if result_type == "bytes":
        return bytes(short_hash)
    else:
        return short_hash
